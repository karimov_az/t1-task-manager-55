package ru.t1.karimov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show command list.";

    @NotNull
    public static final String NAME = "commands";

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for(final AbstractCommand command: commands) {
            if(command == null) continue;
            @NotNull final String name = command.getName();
            if(name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
