package ru.t1.karimov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.domain.DataBinarySaveRequest;
import ru.t1.karimov.tm.enumerated.Role;

@Component
public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save data to binary file.";

    @NotNull
    public static final String NAME = "data-save-bin";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        domainEndpoint.saveDataBinary(new DataBinarySaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
