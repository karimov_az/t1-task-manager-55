package ru.t1.karimov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.domain.DataBinaryLoadRequest;
import ru.t1.karimov.tm.enumerated.Role;

@Component
public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load backup from file.";

    @NotNull
    public static final String NAME = "data-load-bin";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        domainEndpoint.loadDataBinary(new DataBinaryLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
