package ru.t1.karimov.tm.exception.entity;

public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
