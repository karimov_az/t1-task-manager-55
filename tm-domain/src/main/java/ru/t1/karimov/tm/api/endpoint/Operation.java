package ru.t1.karimov.tm.api.endpoint;

import ru.t1.karimov.tm.dto.request.AbstractRequest;
import ru.t1.karimov.tm.dto.response.AbstractResponse;
import ru.t1.karimov.tm.exception.AbstractException;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request) throws AbstractException;

}
