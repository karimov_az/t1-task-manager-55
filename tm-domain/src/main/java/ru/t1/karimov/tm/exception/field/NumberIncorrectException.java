package ru.t1.karimov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class NumberIncorrectException extends AbstractFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect...");
    }

    public NumberIncorrectException(@NotNull final String value, @NotNull Throwable cause) {
        super("Error! Number \"" + value + "\" is incorrect...");
    }

}
