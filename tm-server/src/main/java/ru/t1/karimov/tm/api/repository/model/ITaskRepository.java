package ru.t1.karimov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    Task create(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

}
