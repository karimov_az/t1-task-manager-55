package ru.t1.karimov.tm.api.repository.dto;

import ru.t1.karimov.tm.dto.model.SessionDto;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDto> {
}
