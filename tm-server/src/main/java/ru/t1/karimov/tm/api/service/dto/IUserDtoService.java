package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDto> {

    @NotNull
    UserDto create(
            @Nullable String login,
            @Nullable String password
    ) throws Exception;

    @NotNull
    UserDto create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws Exception;

    @NotNull
    UserDto create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    ) throws Exception;

    @Nullable
    UserDto findByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDto findByEmail(@Nullable String email) throws Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void removeOneByLogin(@Nullable String login) throws Exception;

    void removeOneByEmail(@Nullable String email) throws Exception;

    @NotNull
    UserDto setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    UserDto updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}
