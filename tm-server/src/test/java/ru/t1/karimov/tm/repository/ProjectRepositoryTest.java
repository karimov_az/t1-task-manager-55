package ru.t1.karimov.tm.repository;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.karimov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.karimov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.karimov.tm.api.service.IConnectionService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.dto.model.ProjectDto;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.ProjectSort;
import ru.t1.karimov.tm.marker.UnitCategory;
import ru.t1.karimov.tm.migration.AbstractSchemeTest;
import ru.t1.karimov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.karimov.tm.repository.dto.UserDtoRepository;
import ru.t1.karimov.tm.service.ConnectionService;
import ru.t1.karimov.tm.service.PropertyService;
import ru.t1.karimov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(UnitCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectRepositoryTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final List<ProjectDto> projectList = new ArrayList<>();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static String USER1_ID = "";

    @NotNull
    private static String USER2_ID = "";

    @Nullable
    private static IProjectDtoRepository projectRepository;

    @Nullable
    private static EntityManager entityManager;

    @Nullable
    private static IUserDtoRepository userRepository;

    @BeforeClass
    public static void createUsers() throws Exception {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        @NotNull final UserDto user1 = new UserDto();
        user1.setLogin("test1");
        user1.setPasswordHash(HashUtil.salt(propertyService, "test1"));
        entityManager.getTransaction().begin();
        userRepository.add(user1);
        entityManager.getTransaction().commit();
        USER1_ID = user1.getId();

        @NotNull final UserDto user2 = new UserDto();
        user2.setLogin("test2");
        user2.setPasswordHash(HashUtil.salt(propertyService, "test1"));
        entityManager.getTransaction().begin();
        userRepository.add(user2);
        entityManager.getTransaction().commit();
        USER2_ID = user2.getId();

        entityManager.close();
    }

    @Before
    public void initRepository() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDto project = new ProjectDto();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5) project.setUserId(USER1_ID);
            else project.setUserId(USER2_ID);
            entityManager.getTransaction().begin();
            projectList.add(project);
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        }

        entityManager.close();
    }

    @AfterClass
    public static void clearUsers() throws Exception {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserDtoRepository(entityManager);

        entityManager.getTransaction().begin();
        userRepository.removeOneById(USER1_ID);
        userRepository.removeOneById(USER2_ID);
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    @After
    public void initClear() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (@NotNull final ProjectDto project : projectList) {
            @Nullable final String userId = project.getUserId();
            assertNotNull(userId);
            entityManager.getTransaction().begin();
            projectRepository.removeOneById(userId, project.getId());
            entityManager.getTransaction().commit();
        }
        projectList.clear();

        entityManager.close();
    }

    @Test
    public void testAdd() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        @NotNull final ProjectDto project = new ProjectDto();
        @NotNull final String userId = USER1_ID;
        @NotNull final String name = "Test Project Name";
        @NotNull final String description = "Test Project Description";
        @NotNull final String id = project.getId();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        entityManager.getTransaction().begin();
        projectRepository.add(project);
        entityManager.getTransaction().commit();
        @Nullable final ProjectDto actualProject = projectRepository.findOneById(userId, id);
        assertNotNull(actualProject);
        assertEquals(userId, actualProject.getUserId());
        assertEquals(name, actualProject.getName());
        assertEquals(description, actualProject.getDescription());

        @Nullable final String actualUserId = actualProject.getUserId();
        assertNotNull(actualUserId);
        entityManager.getTransaction().begin();
        projectRepository.removeOneById(actualUserId, actualProject.getId());
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    @Test
    public void testClearForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        @NotNull final List<ProjectDto> emptyList = new ArrayList<>();
        entityManager.getTransaction().begin();
        projectRepository.removeAll(USER1_ID);
        entityManager.getTransaction().commit();
        assertEquals(emptyList, projectRepository.findAll(USER1_ID));
        assertNotEquals(emptyList, projectRepository.findAll(USER2_ID));

        entityManager.close();
    }

    @Test
    public void testClearForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        entityManager.getTransaction().begin();
        projectRepository.removeAll("Other_id");
        entityManager.getTransaction().commit();
        final int numberAllProjects = projectRepository.getSize(USER1_ID).intValue() + projectRepository.getSize(USER2_ID).intValue();
        assertEquals(NUMBER_OF_ENTRIES, numberAllProjects);

        entityManager.close();
    }

    @Test
    public void testFindAllByUser() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        @NotNull final List<ProjectDto> expectedProjects = projectList.stream()
                .filter(m -> USER1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<ProjectDto> actualProjectList = projectRepository.findAll(USER1_ID);
        assertEquals(expectedProjects.size(), actualProjectList.size());

        entityManager.close();
    }

    @Test
    public void testFindAllSortByUser() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<ProjectDto> actualProjectList = projectRepository.findAll(USER1_ID, sort.getComparator());
            @NotNull final List<ProjectDto> expectedProjectList = projectList.stream()
                    .filter(m -> USER1_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            for (int i = 0; i < actualProjectList.size(); i++) {
                @NotNull final ProjectDto actualProject = actualProjectList.get(i);
                @NotNull final ProjectDto expectedProject = expectedProjectList.get(i);
                assertEquals(actualProject.getId(), expectedProject.getId());
            }
        }
        for (@NotNull final ProjectSort sort : ProjectSort.values()) {
            @NotNull final List<ProjectDto> actualProjectList = projectRepository.findAll(USER2_ID, sort.getComparator());
            @NotNull final List<ProjectDto> expectedProjectList = projectList.stream()
                    .filter(m -> USER2_ID.equals(m.getUserId()))
                    .sorted(sort.getComparator())
                    .collect(Collectors.toList());
            for (int i = 0; i < actualProjectList.size(); i++) {
                @NotNull final ProjectDto actualProject = actualProjectList.get(i);
                @NotNull final ProjectDto expectedProject = expectedProjectList.get(i);
                assertEquals(actualProject.getId(), expectedProject.getId());
            }
        }

        entityManager.close();
    }

    @Test
    public void testFindOneByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (@NotNull final ProjectDto project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            @Nullable final ProjectDto actualProject = projectRepository.findOneById(userId, id);
            assertNotNull(actualProject);
            assertEquals(project.getId(), actualProject.getId());
        }

        entityManager.close();
    }

    @Test
    public void testFindOneByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            assertNull(projectRepository.findOneById(USER1_ID, UUID.randomUUID().toString()));
        }

        entityManager.close();
    }

    @Test
    public void testExistByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (@NotNull final ProjectDto project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            assertTrue(projectRepository.existsById(userId, id));
        }

        entityManager.close();
    }

    @Test
    public void testExistByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (@NotNull final ProjectDto project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = UUID.randomUUID().toString();
            assertFalse(projectRepository.existsById(userId, id));
        }

        entityManager.close();
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        @NotNull final List<String> userList = Arrays.asList(USER1_ID, USER2_ID);
        final int expectedSize = NUMBER_OF_ENTRIES / userList.size();
        for (@NotNull final String userId : userList) {
            final int projectRepositorySize = projectRepository.getSize(userId).intValue();
            @Nullable final List<ProjectDto> projectsUser = projectList.stream()
                    .filter(m -> userId.equals(m.getUserId()))
                    .collect(Collectors.toList());
            assertEquals(projectsUser.size(), projectRepositorySize);
            assertEquals(expectedSize, projectRepositorySize);
        }

        entityManager.close();
    }

    @Test
    public void testRemoveByIdForUserPositive() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        for (@NotNull final ProjectDto project : projectList) {
            @Nullable final String userId = project.getUserId();
            @NotNull final String id = project.getId();
            assertNotNull(userId);
            entityManager.getTransaction().begin();
            projectRepository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
            assertNull(projectRepository.findOneById(userId, project.getId()));
        }

        entityManager.close();
    }

    @Test
    public void testRemoveByIdForUserNegative() throws Exception {
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDtoRepository(entityManager);

        final int expectedSize = projectRepository.getSize().intValue();
        @NotNull final String otherUserId = UUID.randomUUID().toString();
        @NotNull final String otherId = UUID.randomUUID().toString();
        entityManager.getTransaction().begin();
        projectRepository.removeOneById(USER1_ID, otherId);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        projectRepository.removeOneById(USER2_ID, otherId);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        projectRepository.removeOneById(otherUserId,otherId);
        entityManager.getTransaction().commit();
        final int actualSize = projectRepository.getSize().intValue();
        assertEquals(expectedSize, actualSize);

        entityManager.close();
    }

}
